FROM maven as build
COPY . .
RUN mvn install -P minify-runnable-package

FROM openjdk:11-jdk
WORKDIR /opt
COPY --from=build /target/getting-started.jar /opt/getting-started.jar

#FROM open-liberty
#LABEL maintainer="Graham Charters" vendor="IBM" github="https://github.com/WASdev/ci.maven"
#COPY --chown=1001:0 --from=server-setup /config/ /config/

CMD java -jar /opt/getting-started.jar
EXPOSE 9080 9443

